import time
import vk
import json
from datetime import datetime
from markov.markov import prepare_text, make_sentence


def filter_words(text, words_lengths):
    if words_lengths != 1:
        new_text = ''
        text_tokens = text.split('.')
        for t in text_tokens:
            if len(t.split(' ')) >= words_lengths:
                new_text += t.strip('.').strip(' ') + '. '
        return new_text


def generate_phrase(path_file, quantity, chains_length=2, words_lengths=1):
    with open(path_file, 'r') as file:
        text = file.read()
    text = filter_words(text, words_lengths)
    words_dict = prepare_text(text, chains_length)
    story = ''
    for i in range(0, quantity):
        story += make_sentence(words_dict) + '\r\n'
    file.close()
    return story


def normalize_people(people):
    return people.lower().replace(' ', '')


def logs_write(message):
    name_logs_file = get_settings_json()['db']['logs_db_file']
    logs = open(name_logs_file, 'a')
    logs.write(datetime.strftime(datetime.now(), "%Y.%m.%d %H:%M:%S") + ' ' + message + '\n')
    logs.close()


def get_settings_json():
    return json.load(open('settings', 'r'))


def get_message_default_phrase():
    return get_settings_json()['messages']['default_phrase']


def get_message_error_log():
    return get_settings_json()['messages']['error_log']


def get_message_error_history():
    return get_settings_json()['messages']['error_history']


def get_message_error_obscene_speech():
    return get_settings_json()['messages']['error_obscene_speech']


def get_message_error_sarcasm():
    return get_settings_json()['messages']['error_sarcasm']


def get_message_error_quote():
    return get_settings_json()['messages']['error_quote']


def get_message_error_phrase():
    return get_settings_json()['messages']['error_phrase']


def get_message_default_history():
    return get_settings_json()['messages']['default_history']


def get_vk_api():
    token = get_settings_json()['token_vk']
    session = vk.Session(token)
    api = vk.API(session)
    return api


def get_admin_id():
    return get_settings_json()['admin_vk']


def get_list_tokens(token_name):
    tokens_file = open(get_settings_json()['tokens'][token_name], 'r')
    tokens = tokens_file.read().strip()
    tokens_file.close()
    return tokens.split('\n')


def get_db_path(db_name):
    return get_settings_json()['db'][db_name]


def get_logs():
    logs_file = open(get_db_path('logs_db_file'), 'r')
    str_logs = logs_file.read()
    logs_file.close()
    return str_logs


def clear_logs():
    logs_file = open(get_db_path('logs_db_file'), 'w')
    logs_file.write('')
    logs_file.close()


def save_to_history(log):
    history_file = open(get_db_path('history_db_file'), 'a')
    history_file.write(log)
    history_file.close()


def find_quantity(people, token, default):
    quantity_find = default
    try:
        if token in people[1].lower():
            c = str(people[1]).replace(' ', '').split(token)[0]
            if c[-1].isdecimal():
                quantity_find = int(c[-1])
                if c[-2].isdecimal():
                    quantity_find = int(c[-2] + c[-1])
    except Exception as exception:
        quantity_find = default
        print('exception in find_quantity with ' + people[0] + ' error: ' + str(exception))
        logs_write('exception in find_quantity with ' + people[0] + ' error: ' + str(exception))
    return quantity_find


def get_param(people, name_param, default=1):
    tokens_list = get_list_tokens(name_param)
    for token in tokens_list:
        speech = normalize_people(people[1])
        if token in speech:
            quantity = find_quantity(people, token, default)
            return quantity
    return default


def message_send(id_user, message):
    get_vk_api().messages.send(user_id=id_user, message=message)
    time.sleep(3)


def reset_logs_into_history():
    logs_str = get_logs()
    clear_logs()
    save_to_history(logs_str)


def entrance_token(people, name_param):
    tokens_list = get_list_tokens(name_param)
    for token in tokens_list:
        speech = normalize_people(people[1])
        if token in speech:
            return True
    return False


def obscene_speech(people):
    for word in people[1].lower().split(' '):
        obscene = get_list_tokens('obscene_speech_tokens_file')
        if word.replace(',', '').replace('.', '').replace('!', '') in obscene:
            message = generate_phrase(get_db_path('obscene_speech_db_file'), 1, 2, 3)
            message_send(people[0], message)
            print('O:' + str(people) + ' ' + message)
            logs_write('O:' + str(people) + ' ' + message)
            return True
    return False


def sentence_send(people, db_path):
    quantity = get_param(people, 'quantity_tokens_file')
    long = get_param(people, 'chain_tokens_file')
    filter_length = get_param(people, 'length_tokens_file', 4)
    path = db_path
    message = generate_phrase(path, quantity, long, filter_length)
    message = message.replace('\n', '\n\n')
    message_send(people[0], message)
    print('O:' + str(people) + ' ' + message)
    logs_write('O:' + str(people) + ' ' + message)


def quote(people):
    if entrance_token(people, 'quote_tokens_file'):
        sentence_send(people, get_db_path('quote_db_path_file'))
        return True
    return False


def phrase(people):
    if entrance_token(people, 'phrase_tokens_file'):
        sentence_send(people, get_db_path('phrase_db_path') + str(people[0]))
        return True
    return False


def sarcasm(people):
    if entrance_token(people, 'sarcasm_tokens_file'):
        message = generate_phrase(get_db_path('sarcasm_db_path_file'), 1, 2, 3)
        message_send(people[0], message)
        print('O:' + str(people) + ' ' + message)
        logs_write('O:' + str(people) + ' ' + message)
        return True
    return False


def logs(people):
    if entrance_token(people, 'logs_tokens_file'):
        if str(people[0]) == get_admin_id():
            message_send(people[0], get_logs())
            return True
    return False


def history(people):
    if entrance_token(people, 'history_tokens_file'):
        if str(people[0]) == get_admin_id():
            logs_write(str(people))
            reset_logs_into_history()
            message_send(people[0], get_message_default_history())
            return True
    return False


def save_run(func, people, error_message_client):
    try:
        if func(people):
            return True
    except Exception as exception:
        logs_write(str(func.__name__) + ' exception with ' + str(people[0]) + ' error: ' + str(exception))
        print(str(func.__name__) + ' exception with ' + str(people[0]) + ' error: ' + str(exception))
        message_send(people[0], error_message_client)
    return False


def action(people):
    if save_run(obscene_speech, people, get_message_error_obscene_speech()):
        return
    if save_run(quote, people, get_message_error_quote()):
        return
    if save_run(phrase, people, get_message_default_phrase()):
        return
    if save_run(sarcasm, people, get_message_error_sarcasm()):
        return
    if save_run(logs, people, get_message_error_log()):
        return
    if save_run(history, people, get_message_error_history()):
        return
    try:
        message_send(people[0], get_message_default_phrase())
        print('O:' + str(people) + ' ' + get_message_default_phrase())
        logs_write(str(people) + ' ' + get_message_default_phrase())
    except Exception as exception:
        print('Exception with ' + str(people[0]) + ' error: ' + str(exception))
        logs_write('Exception with ' + str(people[0]) + ' error: ' + str(exception))
        raise exception


def set_online(delay_set_online):
    if delay_set_online > 300:
        get_vk_api().account.setOnline()
        time.sleep(3)
        logs_write('set online')
        return 0
    else:
        delay_set_online += 1
        return delay_set_online


def accept_friends(delay_accept_friend):
    if delay_accept_friend > 10:
        invites = get_vk_api().friends.getRequests(need_viewed=1)
        for i in invites:
            logs_write('added friend ' + str(i))
            get_vk_api().friends.add(user_id=str(i))
            time.sleep(3)
        return 0
    else:
        delay_accept_friend += 1
        return delay_accept_friend


def get_new_message():
    queue = []
    dialogs = get_vk_api().messages.getDialogs()
    time.sleep(3)
    for dialog in dialogs:
        if dialog == dialogs[0]:
            continue
        if int(dialog['read_state']) == 0 and int(dialog['out']) == 0:
            queue.append([dialog['uid'], dialog['body']])
            mark_message_as_read(dialog['uid'])
    return queue


def mark_message_as_read(id_peer):
    get_vk_api().messages.markAsRead(peer_id=id_peer)
    time.sleep(3)


def main():
    delay_set_online = 300
    delay_accept_friend = 10
    while True:
        delay_set_online = set_online(delay_set_online)
        delay_accept_friend = accept_friends(delay_accept_friend)
        queue = get_new_message()
        for people in queue:
            print('I: ' + str(people))
            logs_write(str(people))
            action(people)


if __name__ == '__main__':
    main()