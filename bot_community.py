import os
import random
import time
import requests
import vk
import json
from datetime import datetime
from markov.markov import prepare_text, make_sentence


def filter_words(text, words_lengths):
    if words_lengths != 1:
        new_text = ''
        text_tokens = text.split('.')
        for t in text_tokens:
            if len(t.split(' ')) >= words_lengths:
                new_text += t.strip('.').strip(' ') + '. '
        return new_text
    return text


def generate_phrase(path_file, quantity, chains_length=2, words_lengths=1):
    with open(path_file, 'r') as file:
        text = file.read()
    text = filter_words(text, words_lengths)
    words_dict = prepare_text(text, chains_length)
    story = ''
    for i in range(1, quantity + 1):
        story += str(i) + ' ' + make_sentence(words_dict) + '\r\n'
    if i == 1:
        story = story[1:]
    file.close()
    return story


def normalize_people(people):
    return people.lower().replace(' ', '')


def logs_write(message):
    name_logs_file = get_settings_json()['db']['logs_db_file']
    logs = open(name_logs_file, 'a')
    logs.write(datetime.strftime(datetime.now(), "%Y.%m.%d %H:%M:%S") + ' ' + message + '\n')
    logs.close()


def get_settings_json():
    return json.load(open('settings', 'r'))


def get_max_post():
    return get_settings_json()['walls']['max_posts']


def get_topic_id():
    return get_settings_json()['walls']['topic_id']


def get_message_help():
    return get_settings_json()['messages']['help_message']


def get_message_unsubscribe_message():
    return get_settings_json()['messages']['unsubscribe_message']


def get_message_completed():
    return get_settings_json()['messages']['message_competed']


def get_error_post_user_on_wall():
    return get_settings_json()['messages']['error_post_user_on_wall']


def get_message_empty_images_folder():
    return get_settings_json()['messages']['empty_images_folder']


def get_message_default_phrase():
    return get_settings_json()['messages']['default_phrase']


def get_message_error_log():
    return get_settings_json()['messages']['error_log']


def get_message_error_history():
    return get_settings_json()['messages']['error_history']


def get_message_error_obscene_speech():
    return get_settings_json()['messages']['error_obscene_speech']


def get_message_error_default():
    return get_settings_json()['messages']['error_default']


def get_message_error_sarcasm():
    return get_settings_json()['messages']['error_sarcasm']


def get_message_error_quote():
    return get_settings_json()['messages']['error_quote']


def get_message_error_phrase():
    return get_settings_json()['messages']['error_phrase']


def get_message_default_history():
    return get_settings_json()['messages']['default_history']


def get_vk_api():
    token = get_settings_json()['token_vk']
    session = vk.Session(token)
    api = vk.API(session)
    return api


def get_vk_api_for_editor():
    token = get_settings_json()['walls']['token_editor']
    session = vk.Session(token)
    api = vk.API(session)
    return api


def get_default_command(id):
    try:
        with open(get_db_path('cfg_users_folder') + id) as cfg:
            cfg_json = json.load(cfg.read())
        return cfg_json
    except:
        return None


def get_cfg_user(user):
    try:
        with open(get_db_path('cfg_users_folder') + str(user)) as cfg:
            return json.loads(cfg.read())
    except:
        return None


def get_admin_id():
    return get_settings_json()['admin_vk']


def get_delay_seconds_post_on_wall():
    return int(get_settings_json()['walls']['delay_seconds_post_on_wall'])


def get_wall_chain_long():
    return int(get_settings_json()['walls']['chain_long'])


def get_wall_source_filter_long():
    return int(get_settings_json()['walls']['source_filter_long'])


def get_wall_message_quantity():
    return int(get_settings_json()['walls']['message_quantity'])


def get_wall_id_editor():
    return int(get_settings_json()['walls']['id_editor'])


def get_wall_id_group():
    return int(get_settings_json()['walls']['group_id'])


def get_list_tokens(token_name):
    tokens_file = open(get_settings_json()['tokens'][token_name], 'r')
    tokens = tokens_file.read().strip()
    tokens_file.close()
    return tokens.split('\n')


def get_db_path(db_name):
    return get_settings_json()['db'][db_name]


def get_path_to_image():
    return get_db_path('folder_pictures')


def get_path_to_image_for_messages():
    return get_db_path('folder_pictures_for_messages')


def get_logs():
    logs_file = open(get_db_path('logs_db_file'), 'r')
    str_logs = logs_file.read()
    logs_file.close()
    return str_logs


def clear_logs():
    logs_file = open(get_db_path('logs_db_file'), 'w')
    logs_file.write('')
    logs_file.close()


def save_to_history(log):
    history_file = open(get_db_path('history_db_file'), 'a')
    history_file.write(log)
    history_file.close()


def get_rnd_image():
    path_image_folder = get_path_to_image()
    image_files = os.listdir(path_image_folder)
    if len(image_files) == 0:
        return None
    rnd_image = path_image_folder + image_files[random.randint(0, len(image_files) - 1)]
    return rnd_image


def get_rnd_image_for_messages():
    path_image_folder = get_path_to_image_for_messages()
    image_files = os.listdir(path_image_folder)
    if len(image_files) == 0:
        return None
    rnd_image = path_image_folder + image_files[random.randint(0, len(image_files) - 1)]
    return rnd_image


def delete_used_image(name):
    if name:
        os.remove(name)


def find_quantity(people, token, default):
    quantity_find = default
    try:
        if token in people[1].lower():
            c = str(people[1]).replace(' ', '').split(token)[0]
            if c[-1].isdecimal():
                quantity_find = int(c[-1])
                if len(c) > 1 and c[-2].isdecimal():
                    quantity_find = int(c[-2] + c[-1])
    except Exception as exception:
        print('exception in find_quantity with ' + str(people[0]) + ' error: ' + str(exception))
        logs_write('exception in find_quantity with ' + str(people[0]) + ' error: ' + str(exception))
        quantity_find = default
    return quantity_find


def get_param(people, name_param, default=1):
    tokens_list = get_list_tokens(name_param)
    for token in tokens_list:
        speech = normalize_people(people[1])
        if token in speech:
            quantity = find_quantity(people, token, default)
            return quantity
    return default


def message_send(id_user, message):
    get_vk_api().messages.send(user_id=id_user, message=message)


def reset_logs_into_history():
    logs_str = get_logs()
    clear_logs()
    save_to_history(logs_str)


def entrance_token(people, name_param):
    tokens_list = get_list_tokens(name_param)
    for token in tokens_list:
        speech = normalize_people(people[1])
        if token in speech:
            return True
    return False


def obscene_speech(people):
    for word in people[1].lower().split(' '):
        obscene = get_list_tokens('obscene_speech_tokens_file')
        if word.replace(',', '').replace('.', '').replace('!', '') in obscene:
            message = generate_phrase(get_db_path('obscene_speech_db_file'), 1, 2, 3)
            message_send(people[0], message)
            print('O:' + str(people) + ' ' + message)
            logs_write('O:' + str(people) + ' ' + message)
            return True
    return False


def sentence_send(people, db_path):
    quantity = get_param(people, 'quantity_tokens_file')
    if entrance_token(people, 'chain_tokens_file'):
        long = 2
    else:
        long = 1
    long = get_param(people, 'chain_tokens_file', long)
    filter_length = get_param(people, 'length_tokens_file', 4)
    path = db_path
    message = generate_phrase(path, quantity, long, filter_length)
    message = message.replace('\n', '\n\n')
    message_send(people[0], message)
    print('O:' + str(people) + ' ' + message)
    logs_write('O:' + str(people) + ' ' + message)


def sentence_send_with_image(people, db_path):
    quantity = get_param(people, 'quantity_tokens_file')
    long = get_param(people, 'chain_tokens_file')
    filter_length = get_param(people, 'length_tokens_file', 4)
    path = db_path
    message = generate_phrase(path, quantity, long, filter_length)
    message = message.replace('\n', '\n\n')
    uri = get_vk_api().photos.getMessagesUploadServer()['upload_url']
    response = requests.post(uri, files={'photo': open(get_rnd_image_for_messages(), 'rb')}).json()
    response_vk = get_vk_api().photos.saveMessagesPhoto(server=response['server'], photo=response['photo'], hash=response['hash'])
    attach = ('photo'+str(response_vk[0]['owner_id'])+'_'+str(response_vk[0]['pid'])).strip('\'')
    get_vk_api().messages.send(user_id=people[0], message=message, attachment=attach)
    print('O:' + str(people) + ' ' + message)
    logs_write('O:' + str(people) + ' ' + message)


def quote(people):
    if entrance_token(people, 'quote_tokens_file'):
        sentence_send(people, get_db_path('quote_db_path_file'))
        return True
    return False


def quote_picture(people):
    if entrance_token(people, 'quote_image_tokens_file'):
        sentence_send_with_image(people, get_db_path('quote_db_path_file'))
        return True
    return False


def phrase(people):
    if entrance_token(people, 'phrase_tokens_file'):
        sentence_send(people, get_db_path('phrase_db_path') + str(people[0]))
        return True
    return False


def sarcasm(people):
    if entrance_token(people, 'sarcasm_tokens_file'):
        message = generate_phrase(get_db_path('sarcasm_db_path_file'), 1, 2, 3)
        message_send(people[0], message)
        print('O:' + str(people) + ' ' + message)
        logs_write('O:' + str(people) + ' ' + message)
        return True
    return False


def logs(people):
    if entrance_token(people, 'logs_tokens_file'):
        if str(people[0]) == get_admin_id():
            message_send(people[0], get_logs())
            return True
    return False


def history(people):
    if entrance_token(people, 'history_tokens_file'):
        if str(people[0]) == get_admin_id():
            logs_write(str(people))
            reset_logs_into_history()
            message_send(people[0], get_message_default_history())
            return True
    return False


def check_on_subscribe(id):
    if str(get_vk_api().groups.isMember(group_id=get_wall_id_group(), user_id=id[0])) == '1':
        return True
    else:
        raise Exception('unsubscribe')


def more_message(people):
    if entrance_token(people, 'more_tokens'):
        people[1] = get_cfg_user('last_command')
        return save_run(quote, people, get_message_error_quote())
    else:
        return False


def on_always(people):
    if entrance_token(people, 'on_answer_on_all'):
        update_cfg_param(people, 'answer_on_all', 1)
        message_send(people[0], get_message_completed() + people[1])
        return True
    return False


def help_message(people):
    if entrance_token(people, 'help_tokens_file'):
        message = get_message_help()
        message_send(people[0], message)
        print('O:' + str(people) + ' ' + message)
        logs_write('O:' + str(people) + ' ' + message)
        return True
    return False


def off_always(people):
    if entrance_token(people, 'off_answer_on_all'):
        update_cfg_param(people, 'answer_on_all', 0)
        message_send(people[0], get_message_completed() + people[1])
        return True
    return False


def save_run(func, people, error_message_client):
    try:
        return func(people)
    except Exception as exception:
        logs_write(str(func.__name__) + ' exception with ' + str(people[0]) + ' error: ' + str(exception))
        print(str(func.__name__) + ' exception with ' + str(people[0]) + ' error: ' + str(exception))
        message_send(people[0], error_message_client)
    return False


def save_cfg_user(people, template):
    with open(get_db_path('cfg_users_folder') + str(people[0]), 'w') as cfg:
        json_file = json.dumps(template)
        cfg.write(json_file.replace('\'', '\"').strip('\"'))
    return json.loads(json_file)


def update_cfg_param(people, name_param, value):
    cfg_file = get_cfg_user(people[0])
    if str(value).strip(' ') == '':
        value = 'woop'
    cfg_file = json.loads(str(cfg_file).replace(str(cfg_file[name_param]), str(value)).strip('\'').replace('\'', '\"'))
    save_cfg_user(people, str(cfg_file))
    return cfg_file


def init_cfg_user(people):
    cfg_file = get_cfg_user(people[0])
    if not cfg_file:
        template = {
            'answer_on_all': 0,
            'last_command': 'woooop',
            'post_count': 0
        }
        cfg_file = save_cfg_user(people, template)
    return cfg_file


def check_on_permission_post_on_wall(people):
    with open(get_db_path('moderations_file'), 'r') as list_file:
        list_moders = list_file.read().strip('\n').split('\n')
    return str(people[0]) in list_moders


def find_number_message(message, number):
    if number < 0:
        return message.split('<br><br>')[0]
    number -= 1
    message_filtered = message.split('<br><br>')[number]
    cut_number = 1
    for i in range(2):
        if str(message_filtered[i]).isdecimal():
            cut_number += 1
    return message_filtered[2:]


def post_on_wall_from_user_with_picture(people):
    if entrance_token(people, 'quote_picture') and check_on_permission_post_on_wall(people):
        image = get_rnd_image()
        if image:
            finded_message = get_vk_api().messages.getHistory(user_id=str(people[0]), count=2)[2]
            if str(finded_message['uid']) == ('-'+str(get_wall_id_group())):
                phrase_str = find_number_message(finded_message['body'], get_param(people, 'number_message_token_file', -1))
                uri = get_vk_api_for_editor().photos.getWallUploadServer(uid=get_wall_id_editor(), gid=get_wall_id_group())[
                        'upload_url']
                response = requests.post(uri, files={'photo': open(image, 'rb')}).json()
                response_vk = get_vk_api_for_editor().photos.saveWallPhoto(group_id=get_wall_id_group(),
                                                                               server=response['server'], photo=response['photo'],
                                                                               hash=response['hash'])
                get_vk_api_for_editor().wall.post(owner_id='-' + str(get_wall_id_group()), from_group='1', message=phrase_str,
                                                      attachments=response_vk[0]['id'])
                message_send(people[0], get_message_completed() + people[1])
                delete_used_image(image)
            else:
                message_send(people[0], get_error_post_user_on_wall())
        else:
            message_send(people[0], get_message_empty_images_folder())
        return True
    return False


def null_timers(people):
    if datetime.now().hour == 0 and datetime.now().minute == 0:
        update_cfg_param(people, 'post_count', 0)


def post_on_wall_from_user(people):
    if entrance_token(people, 'quote_added_to_wall'):
        if check_on_permission_post_on_wall(people):
            finded_message = get_vk_api().messages.getHistory(user_id=str(people[0]), count=2)[2]
            if str(finded_message['uid']) == ('-' + str(get_wall_id_group())):
                phrase_str = find_number_message(finded_message['body'],
                                                 get_param(people, 'number_message_token_file', -1))
                get_vk_api_for_editor().wall.post(owner_id='-' + str(get_wall_id_group()), from_group='1',
                                                  message=phrase_str)
                message_send(people[0], get_message_completed() + people[1])
            else:
                message_send(people[0], get_error_post_user_on_wall())
        else:
            finded_message = get_vk_api().messages.getHistory(user_id=str(people[0]), count=2)[2]
            if str(finded_message['uid']) == ('-' + str(get_wall_id_group())) and get_cfg_user(people[0])['post_count'] <= int(get_max_post()):
                phrase_str = find_number_message(finded_message['body'],
                                                 get_param(people, 'number_message_token_file', -1))
                get_vk_api_for_editor().board.createComment(group_id=get_wall_id_group(), topic_id=int(get_topic_id()),
                                                            message=phrase_str, from_group=1)
                update_cfg_param(people, 'post_count', (get_cfg_user(people[0])['post_count'] + 1))
                message_send(people[0], get_message_completed() + people[1])
            else:
                message_send(people[0], get_error_post_user_on_wall())
        return True
    return False


def process_on_user(people):
    cfg_file = get_cfg_user(people[0])
    if str(cfg_file['answer_on_all']) == '1':
        people[1] = cfg_file['last_command']
    elif entrance_token(people, 'more_tokens'):
        people[1] = cfg_file['last_command']
    else:
        update_cfg_param(people, 'last_command', people[1])
    return people


def action(people):
    people[1] = people[1].replace('answer_on_all', 'woop').replace('last_command', 'woop')
    # if not save_run(check_on_subscribe, people, get_message_unsubscribe_message()):
    #     return
    init_cfg_user(people)
    null_timers(people)

    if save_run(help_message, people, get_message_error_default()):
        return
    if save_run(off_always, people, get_message_error_default()):
        return
    if save_run(on_always, people, get_message_error_default()):
        return
    if save_run(post_on_wall_from_user_with_picture, people, get_message_error_default()):
        return
    if save_run(post_on_wall_from_user, people, get_message_error_default()):
        return
    people = process_on_user(people)
    if save_run(quote_picture, people, get_message_error_quote()):
        return
    if save_run(obscene_speech, people, get_message_error_obscene_speech()):
        return
    if save_run(quote, people, get_message_error_quote()):
        return
    if save_run(phrase, people, get_message_default_phrase()):
        return
    if save_run(sarcasm, people, get_message_error_sarcasm()):
        return
    if save_run(logs, people, get_message_error_log()):
        return
    if save_run(history, people, get_message_error_history()):
        return
    if save_run(more_message, people, get_message_error_default()):
        return
    try:
        message_send(people[0], get_message_default_phrase())
        print('O:' + str(people) + ' ' + get_message_default_phrase())
        logs_write(str(people) + ' ' + get_message_default_phrase())
    except Exception as exception:
        print('Exception with ' + str(people[0]) + ' error: ' + str(exception))
        logs_write('Exception with ' + str(people[0]) + ' error: ' + str(exception))
        raise exception


def get_new_message():
    queue = []
    dialogs = get_vk_api().messages.getDialogs()
    for dialog in dialogs:
        if dialog == dialogs[0]:
            continue
        if int(dialog['read_state']) == 0 and int(dialog['out']) == 0:
            queue.append([dialog['uid'], dialog['body']])
            mark_message_as_read(dialog['uid'])
    return queue


def mark_message_as_read(id_peer):
    get_vk_api().messages.markAsRead(peer_id=id_peer)


def post_on_wall_message(image):
    quantity = get_wall_message_quantity()
    chain_length = get_wall_chain_long()
    ph_long = get_wall_source_filter_long()
    phrase_str = generate_phrase(get_db_path('quote_db_path_file'), quantity, chain_length, ph_long)
    if image:
        uri = get_vk_api_for_editor().photos.getWallUploadServer(uid=get_wall_id_editor(), gid=get_wall_id_group())['upload_url']
        response = requests.post(uri, files={'photo': open(image, 'rb')}).json()
        response_vk = get_vk_api_for_editor().photos.saveWallPhoto(group_id=get_wall_id_group(), server=response['server'], photo=response['photo'], hash=response['hash'])
        get_vk_api_for_editor().wall.post(owner_id='-' + str(get_wall_id_group()), from_group='1', message=phrase_str, attachments=response_vk[0]['id'])
    else:
        get_vk_api_for_editor().wall.post(owner_id='-' + str(get_wall_id_group()), from_group='1', message=phrase_str)


def post_on_wall_main():
    image = get_rnd_image()
    post_on_wall_message(image)
    delete_used_image(image)
    print('Posted on wall')
    logs_write('Posted on wall')


def timer_post_on_wall(time_delay):
    if time_delay >= get_delay_seconds_post_on_wall():
        try:
            post_on_wall_main()
        except Exception as e:
            print('Error post on wall ' + str(e))
            logs_write('Error post on wall ' + str(e))
        time_delay = 0
    else:
        time_delay += 1
    return time_delay


def get_lat_timer(name_cfg):
    with open(name_cfg, 'r') as file:
        numbers = file.read()
        if numbers.isdecimal():
            return int(numbers)
        else:
            return 0


def write_timer(name_cfg, timer):
    with open(name_cfg, 'w') as file:
        file.write(str(timer))


def main():
    # delay_time = timer_post_on_wall(get_lat_timer(get_db_path('timer_file')))
    while True:
        # delay_time = timer_post_on_wall(delay_time)
        # write_timer(get_db_path('timer_file'), delay_time)
        queue = get_new_message()
        for people in queue:
            print('I: ' + str(people))
            logs_write(str(people))
            action(people)
        time.sleep(1)


if __name__ == '__main__':
    main()