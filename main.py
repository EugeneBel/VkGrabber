import time
import vk
import sys

acess_token = ""
user = 1
messages_count = 200

if len(sys.argv) > 3:
    acess_token = str(sys.argv[1])
    user = str(sys.argv[2])
    messages_count = int(sys.argv[3])

command = "usr"

if len(sys.argv) > 4:
    command = str(sys.argv[4])

if command == "usr":
    session = vk.Session(acess_token)
    api = vk.API(session)
    for i in range(0, messages_count, 200):
        text = ''
        print(str(round(i / messages_count, 2) * 100)+'%')
        messages = api.messages.getHistory(user_id=user, count = 200, offset = i)
        for message in messages:
            if message == messages[0]:
                continue
            if str(message['from_id']) == str(user) and len(message['body']) > 2:
                l = message['body'][0].upper() + message['body'][1:] + '. '
                if len(l.split(' ')) > 2:
                    mes = message['body'].replace('.','').replace('!','').replace('?','').replace(',','').strip()
                    text += mes[0].upper() + mes[1:].lower() + '. '
        f = open('text.txt', 'a')
        f.write(text.replace('<br>','\n'))
        f.close()
        time.sleep(3)

if 'community' in command:
    session = vk.Session(acess_token)
    api = vk.API(session)
    for i in range(0, messages_count, 100):
        text = ''
        print(str(round(i / messages_count, 2) * 100) + '%')
        if 'Owner' in command:
            messages = api.wall.get(owner_id=('-'+user), count=100, offset=i)
        else:
            messages = api.wall.get(domain=user, count=100, offset=i)
        for message in messages:
            if message == messages[0]:
                continue
            if len(message['text']) > 2:
                l = message['text'][0].upper() + message['text'][1:] + '. '
                if len(l.split(' ')) > 2:
                    mes = message['text'].replace('.','').replace('!','').replace('?','').replace(',','').strip()
                    text += mes[0].upper() + mes[1:].lower() + '. '
        f = open('text.txt', 'a')
        f.write(text.replace('<br>','\n'))
        f.close()
        time.sleep(3)
