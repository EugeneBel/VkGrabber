import sys
import os

path = '/'
length_phrases = 2

if len(sys.argv) > 1:
    length_phrases = int(sys.argv[1])

if len(sys.argv) > 2:
    path = str(sys.argv[2])

files_array = os.listdir(path)

print('Founded files:')
print(files_array)

for i in files_array:
    print('Process on file: ' + i)
    file_r = open(path + i, 'r')
    string_in = file_r.read()
    file_r.close()
    os.remove(path + i)
    file_w = open(path + i, 'w')
    tokens_str = string_in.replace('\n', '.').replace('!', '.').replace('?', '.').split('.')
    print('Input count: ' + str(len(tokens_str)))
    out_count = 0
    for j in tokens_str:
        if len(j.strip(' ').split(' ')) >= length_phrases:
            file_w.write(j.replace('.', '').strip(' ') + '. ')
            out_count += 1
    print('Wrote count: ' + str(out_count))
    print('Deleted: ' + str((len(tokens_str) - out_count)))
    file_w.close()
