import time
import bot_community

intern = 0
run = False
while True:
    if intern <= 0:
        try:
            print('Start')
            bot_community.main()
        except Exception as e:
            print('Down server, restart')
            print(str(e))
            intern = 60
    else:
        print(intern)
        intern -= 1
        time.sleep(1)
