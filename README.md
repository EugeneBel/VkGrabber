# VkGrabber  
  
It is bot and saver history dialogs and wall community in txt format  
For use in install vk modules  
``` bash
pip3 install vk
```
Then for save history dialog:  
``` bash
python3 main.py token user_id quantity
```
Where token - your access token for vk, user_id - your collocutor, quantity - number messages (9999999999 - save all messages)  
Or save community wall:  
``` bash
python3 main.py token name quantity community
```
Where token - your access token for vk, name - title community, quantity - number messages on wall (9999999999 - save all messages)  
  
# Bot  
For start your bot you can get latest release https://gitlab.com/2DKot/markov-chains/ and put on folder markov.          
For run bot file:  
``` bash
python3 bot.py  
```
For run bot with auto start after error:
``` bash
python3 runner.py  
```

# Settings
Take settings_template and rename to settings.  
Open and edit params:   
`token_vk` - your token vk for access    
`admin_vk` - your id page in vk    
In `tokens` contains keys to path file with tokens
In `db` contains keys to path file with database  
In `messages` contains keys to default phrase on different situation